﻿
namespace TestProject1
{
    [TestClass]
    public class EncodeDecodeTest
    {
        [TestMethod]
        public void TestEncode() {
            string stringToProve = "marte roja";
            string encode = EncodeDecodeLibrary.Scripts.Encode(stringToProve);
            Assert.AreEqual("bWFydGUgcm9qYQ==", encode);
        }

        [TestMethod]
        public void TestEncodeNull()
        {
            string stringToProve = "";
            string encode = EncodeDecodeLibrary.Scripts.Encode(stringToProve);
            Assert.AreEqual("", encode);
        }
        [TestMethod]
        public void TestDecode()
        {
            string stringToProve = "bWFydGUgcm9qYQ==";
            string encode = EncodeDecodeLibrary.Scripts.Decode(stringToProve);
            Assert.AreEqual("marte roja", encode);
        }
        [TestMethod]
        public void TestDecodeNull()
        {
            string stringToProve = "";
            string encode = EncodeDecodeLibrary.Scripts.Decode(stringToProve);
            Assert.AreEqual("", encode);
        }



    }
}
