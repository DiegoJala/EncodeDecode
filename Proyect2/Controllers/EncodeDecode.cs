﻿using Microsoft.AspNetCore.Mvc;
using EncodeDecodeLibrary;
namespace Proyect2.Controllers;

[ApiController]
[Route("Api")]
public class EncodeDecodeController : ControllerBase
{

    [HttpPost("Encode")]
    public dynamic Endoce([FromBody] Models.EncodeDecode model)
    {
        
        string encoded = Scripts.Encode(model.Str);
        ;
        return new
        {
            Str = encoded
        };
    }

    [HttpPost("Decode")]
    public dynamic Decode([FromBody] Models.EncodeDecode model)
    {

        string decoded = Scripts.Decode(model.Str);
        ;
        return new
        {
            Str = decoded
        };
    }
}
